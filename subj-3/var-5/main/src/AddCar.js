import React from 'react';

export class AddCar extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            make: '',
            model: '',
            price: ''
        }
    }

    addCar = () => {
        let car = {
            make: this.state.make,
            model: this.state.model,
            price: this.state.price
        };
        this.props.onAdd(car);
    }

    handleChangeMake = (el) => {
        this.setState({
            make: el.target.value
        })
    }

    handleChangeModel = (el) => {
        this.setState({
            model: el.target.value
        })
    }

    handleChangePrice = (el) => {
        this.setState({
            price: el.target.value
        })
    }

    render(){
        return (
            <div>
                <input id="make" onChange={this.handleChangeMake} name="make"></input>
                <input id="model" onChange={this.handleChangeModel} name="model"></input>
                <input id="price" onChange={this.handleChangePrice} name="price"></input>
                <button value="add car" onClick={this.addCar}>Add car</button>
            </div>
        )
    }
}

export default AddCar;